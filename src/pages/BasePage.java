package pages;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	public WebDriver driver;
	public WebDriverWait wait;
	
	//constructor
	public BasePage (WebDriver driver, WebDriverWait wait) throws Exception{
		try{
		this.driver = driver;
		this.wait = wait;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}
	
	/*
	 * Method to get doms for pages from properties files
	 * Returns : HashMap of all the Doms used for automation for that locale
	 */
	public HashMap<String, String> getPageSpecificDomsFromPropFile() throws Exception{
		Properties prop = new Properties();
		HashMap<String, String> domMap = new HashMap<String,String>();
		try{
			String domsProp = "./src/config/";
		    domsProp +="doms"+".properties";
		
		InputStream locStream = new FileInputStream(domsProp);
		prop.load(locStream);
		int domSize = prop.size();
	
		for (final String name: prop.stringPropertyNames()){
			domMap.put(name, prop.getProperty(name));
		}
		/*
		 * forLoop is commented only to keep the output clean. a very useful 
		 * loop that will Iterate the map and spit out the KV pairs. Run locally
		 * to verify if the test has all hte elements to act on.
		 */
		/* for (Entry e: domMap.entrySet()){
		    	System.out.println("Key : "+ e.getKey()+ "  Value : "+e.getValue());
		    }*/

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return domMap;
	}
	
	
	
	
}// end base class
