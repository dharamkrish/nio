package pages;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class GooglePage extends BasePage{
	HashMap <String,String> DomsMap = new HashMap<String,String>();
	public GooglePage(WebDriver driver, WebDriverWait wait) throws Exception{
		super(driver, wait);	
		DomsMap = super.getPageSpecificDomsFromPropFile(); //First - Load the elements for the page depending on Locale..
		if(DomsMap.isEmpty() || DomsMap == null){
			Assert.assertNotNull(DomsMap, "The DOM MAP IS EMPTY OR NULL... Stopping Execution..");	
		}
		getPathFromHashMap();
		
	}
	String searchBoxXpath,typeAheadXPath,DownloadFireFoxXpath,googleURL;
	public void getPathFromHashMap() throws Exception {
		searchBoxXpath = DomsMap.get("searchBoxXpath");
		typeAheadXPath = DomsMap.get("typeAheadXPath");
		DownloadFireFoxXpath = DomsMap.get("DownloadFireFoxXpath");
		googleURL = DomsMap.get(googleURL);

	}
	public boolean goToGoogle() throws Exception{
		try{
		
			if (googleURL == null || googleURL.isEmpty()) {
				
				return false;
			}
			driver.get(googleURL);
		}catch(Exception ex){
			ex.fillInStackTrace();
			return false;
		}
		return true;
	}// end method
	
	
}// class ends
