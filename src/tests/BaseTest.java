package tests;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.apache.commons.lang3.SystemUtils;

public class BaseTest {
	static  WebDriver driver;
	public WebDriverWait wait;
	private String browserName;
	private String browser ="";
	
	@Parameters ({" "})
    @BeforeTest
    @BeforeClass
    public void setup() {

		String browser = browserName;
		try {
		// Detect the OS where the test is running to ensure jenkins runs the right driver
		String currentOS = getOsType();
		if(currentOS.equalsIgnoreCase("MAC")){
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverMac");
			//System.setProperty("webdriver.gecko.driver", "./Driver/geckodriverMAC");
			//FirefoxDriver driver = new FirefoxDriver();
			ChromeDriver driver = new ChromeDriver();
				driver = new ChromeDriver();
				driver.manage().window().fullscreen();
		}
		if(currentOS.equalsIgnoreCase("windows")){
			System.setProperty("webdriver.chrome.driver", "./Driver/chromedriverWIN.exe");
			ChromeDriver driver = new ChromeDriver();
				driver = new ChromeDriver();
				driver.manage().window().fullscreen();
		}
		
		}catch (Exception ex){
			ex.printStackTrace();
		}
	
	}
	
	public String getOsType(){
		if(SystemUtils.IS_OS_WINDOWS){
			System.out.println("DETECTED OS = WINDOWS");
			return "windows";}
		if(SystemUtils.IS_OS_LINUX){
			System.out.println("DETECTED OS = LINUX");
			return "linux";}
		if(SystemUtils.IS_OS_MAC){
			System.out.println("DETECTED OS = MAC");
			return "mac";
		}
			
		return "no OS detected";
	}
}
