package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import pages.GooglePage;

public class TestGooglePage extends BaseTest{
@Test
public void navigateAndVerifyInGooglePage() throws Exception {
	try {
	GooglePage googleHome = new GooglePage(driver, wait);
	boolean isPass = false;
	isPass = googleHome.goToGoogle();
	Assert.assertEquals(isPass, true);
	
	
	
	}catch (Exception ex) {
		ex.printStackTrace();
	}
}
}
